# print details from all pods:
kubectl get pods | awk '{print $1}' | xargs -I{} kubectl exec {} -- <command goes here>

# print pod name and podIP:
kubectl get pods | awk '{print $1}' | xargs -I{} kubectl get po {} --template='{{.metadata.name}} {{printf "%s\n" .status.podIP}}'

# set new image for deployment container
kubectl set image deploy/<deployment> <container>=<image-url>

# expose deployment as services
kubectl expose deployment/<deployment-name> --port=<port-number> --type=NodePort

# scaling pods
kubectl scale deployment/<pod-name> --replicas=N

# ------------------------ POD Upgrade & History ---------------
# list history of deployments
kubectl rollout history deployment/<deployment-name>

# jump to specific revision
kubectl rollout undo deployment/<deployment-name> --to-revision=N

# ------------------------- Delete Pods ------------------------

# force deletion of Pods
kubectl delete pods <pod> --grace-period=0 --force

# delete all evicted pods from current namespace
kubectl get pods | grep Evicted | awk '{print $1}' | xargs kubectl delete pod
kubectl delete pods --field-selector=status.phase=Evicted

# delete pods in pending phase
kubectl get pods --field-selector=status.phase=Pending
 
# delete all evicted pods from all namespaces
kubectl get pods --all-namespaces | grep Evicted | awk '{print $2 " --namespace=" $1}' | xargs kubectl delete pod

# delete all containers in ImagePullBackOff state from all namespaces
kubectl get pods --all-namespaces | grep 'ImagePullBackOff' | awk '{print $2 " --namespace=" $1}' | xargs kubectl delete pod

# delete all containers in ImagePullBackOff or ErrImagePull or Evicted state from all namespaces
kubectl get pods --all-namespaces | grep -E 'ImagePullBackOff|ErrImagePull|Evicted' | awk '{print $2 " --namespace=" $1}' | xargs kubectl delete pod

# To remove failed jobs:
kubectl delete job $(kubectl get jobs | awk '$3 ~ 0' | awk '{print $1}')

# To remove completed jobs:
kubectl delete job $(kubectl get jobs | awk '$3 ~ 1' | awk '{print $1}')


# ------------------- Logs From Kubernetes Pods ----------------------------

# print logs from all pods:
kubectl get pods | grep <filter-param> | awk '{print $1}' | xargs -I{} kubectl logs {} --since 1h

# watch logs of latest Job in CronJob:
kubectl logs $(k get pods | grep <filter-param>  | tail -1 |  awk '{print $1}')

# watch application logs from pod
kubectl exec -it <pod-name> -c <container-name> -- tail -f /usr/src/app/logs/application.log

# ------------------ Kubernetes Secrets and ConfigMaps -----------------------

# create kubernetes secret from file:
kubectl create secret generic <secret-name> --from-file=<variable-name>=<file-path>

# create kubernetes configmap from value:
kubectl create configmap <config-map-name> --from-literal=<Name>=<value>
kubectl create configmap <config-map-name> --from-file=config.js

