# 1. Navigate to http://play-with-docker.com/

# 2. Initialize the swarm on node 1(Assign it as manager node)
docker swarm init --advertise-addr eth0

# 3. To add a worker to this swarm cluster, copy output of init & run it on other nodes

# 4. Go to node1(list manager & worker nodes)
docker node ls

# 5. Deploy Nginx service
docker service create --detach=true --name nginx1 --publish 80:80
  --mount source=/etc/hostname,target=/usr/share/nginx/html/index.html,type=bind,ro nginx:1.12

# 6. Inspect services in swarm cluster
docker service ls

# 7. Check the running container of the nginx1 service
docker service ps nginx1

# 8. Test the service
curl localhost:80

# 9. Update number of replicas to nginx1 service
docker service update --replicas=5 --detach=true nginx1

# 10. Check the running instances for nginx1 service
docker service ps nginx1

# 11. Test the service
curl localhost:80

# 12. Check the aggregated logs for the nginx1 service
docker service logs nginx1

# 13. Apply new image to nginx service
docker service update --image nginx:1.13 --detach=true nginx1
  # You can fine-tune the rolling update by using these options:
  #     --update-parallelism: specifies the number of containers to update immediately (defaults to 1).
  #     --update-delay: specifies the delay between finishing updating a set of containers before moving on to the next set.

# 14. Watch rolling updates
watch -n 1 docker service ps nginx1

# Swarm handles reconcile problems with containers by recognizing the mismatch between desired state and actual state

# 15a. Navigate to node3
docker swarm leave

# 15b. kill the node3

# 16. Watch rolling updates
watch -n 1 docker service ps nginx1
